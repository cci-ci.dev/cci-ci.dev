ARG BASE_BUILD_ENV


FROM quay.io/centos/centos:stream as centos_stream_env
RUN yum update -y && \
    yum install -y \
        cmake \
        make \
        git \
        gcc \
        gcc-c++ \
        python3 \
        python3-pip && \
    pip3 install conan && \
    conan config install https://github.com/conan-io/hooks.git && \
    conan config set hooks.conan-center
RUN conan profile new default --detect && \
    conan profile update settings.compiler.libcxx=libstdc++11 default;


FROM alpine as alpine_env
RUN apk add \
        cmake \
        gcc \
        g++ \
        git \
        make \
        perl \
        linux-headers \
        python3 \
        py3-pip  && \
    pip3 install conan && \
    conan config install https://github.com/conan-io/hooks.git && \
    conan config set hooks.conan-center
RUN conan profile new default --detect && \
    conan profile update settings.compiler.libcxx=libstdc++11 default;


FROM ubuntu as ubuntu_env
ENV DEBIAN_FRONTEND=noninteractive
RUN apt-get update -y && \
    apt-get install -y \
        autoconf \
        build-essential \
        cmake \
        git \
        libgl1-mesa-dev \
        pkg-config \
        python3 \
        python3-pip && \
    pip3 install conan && \
    conan config install https://github.com/conan-io/hooks.git && \
    conan config set hooks.conan-center
RUN conan profile new default --detect && \
    conan profile update settings.compiler.libcxx=libstdc++11 default;


FROM ubuntu as ubuntu_clang_env
ENV DEBIAN_FRONTEND=noninteractive
ENV CC=/usr/bin/clang
ENV CXX=/usr/bin/clang++
RUN apt-get update -y && \
    apt-get install -y \
        autoconf \
        build-essential \
        cmake \
        clang \
        git \
        libgl1-mesa-dev \
        pkg-config \
        python3 \
        python3-pip && \
    pip3 install conan && \
    conan config install https://github.com/conan-io/hooks.git && \
    conan config set hooks.conan-center
RUN apt-get install -y \
	--no-install-recommends \
	libfontenc-dev \
	libice-dev \
	libsm-dev \
	libx11-xcb-dev \
	libxaw7-dev \
	libxcb-dri3-dev \
	libxcb-icccm4-dev \
	libxcb-image0-dev \
	libxcb-keysyms1-dev \
	libxcb-randr0-dev \
	libxcb-render-util0-dev \
	libxcb-render0-dev \
	libxcb-shape0-dev \
	libxcb-sync-dev \
	libxcb-util-dev \
	libxcb-xfixes0-dev \
	libxcb-xinerama0-dev \
	libxcb-xkb-dev \
	libxcomposite-dev \
	libxcursor-dev \
	libxdamage-dev \
	libxext-dev \
	libxfixes-dev \
	libxft-dev \
	libxi-dev \
	libxinerama-dev \
	libxkbfile-dev \
	libxmu-dev \
	libxmuu-dev \
	libxpm-dev \
	libxrandr-dev \
	libxrender-dev \
	libxres-dev \
	libxss-dev \
	libxt-dev \
	libxtst-dev \
	libxv-dev \
	libxvmc-dev \
	libxxf86vm-dev \
	xkb-data
RUN conan profile new default --detect && \
    conan profile update settings.compiler.libcxx=libstdc++11 default;


FROM centos:7 as centos_7_devtool_set_10
RUN yum update -y && \
    yum install -y \
        epel-release \
        centos-release-scl && \
    yum install -y \
        autoconf \
        automake \
        bison \
        bzip2 \
        cmake3 \
        devtoolset-10 \
        devtoolset-10-libasan-devel \
        devtoolset-10-liblsan-devel \
        devtoolset-10-libtsan-devel \
        devtoolset-10-libubsan-devel \
        dpkg \
        fakeroot \
        flex \
        fontconfig-devel \
        freetype-devel \
        gettext \
        intltool \
        libtool \
        python3 \
        python3-pip \
        make \
        ninja-build \
        patchelf \
        pkgconfig
RUN ln -s /usr/bin/cmake3 /usr/local/bin/cmake && \
    ln -s /usr/bin/ctest3 /usr/local/bin/ctest && \
    ln -s /usr/bin/cpack3 /usr/local/bin/cpack
SHELL ["/usr/bin/scl", "enable", "devtoolset-10"]
RUN pip3 install conan && \
    conan config install https://github.com/conan-io/hooks.git && \
    conan config set hooks.conan-center && \
    conan profile new default --detect


ARG BASE_BUILD_ENV
FROM ${BASE_BUILD_ENV} as build_env
WORKDIR /usr/local/src
COPY conan-center-index conan-center-index
