#!/usr/bin/env python3
import json
from itertools import product
import random
from dataclasses import dataclass
from typing import Dict, List
import argparse
from importlib import import_module
import yaml
import os
import inspect
from conans.model.conan_file import ConanFile


@dataclass
class Package:
    name: str
    subfolder: str
    version: str
    options: Dict[str, str]


def get_all_packages(recipe_dir: str):
    # print(f"Handle {recipe_dir}")
    packages = []
    recipe_dirname = os.path.basename(os.path.realpath(recipe_dir))


    recipe_config = yaml.load(open(os.path.join(recipe_dir, 'config.yml')), Loader=yaml.FullLoader)
    for version, version_description in recipe_config['versions'].items():
        version_dir = version_description['folder']
        path_to_conanfile = f"conan-center-index.recipes.{recipe_dirname}.{version_dir}.conanfile"
        python_code = import_module(path_to_conanfile)


        conanfile_class = None
        for name, obj in inspect.getmembers(python_code):
            if inspect.isclass(obj) and ConanFile in obj.__bases__:
                conanfile_class = obj
        assert conanfile_class is not None

        options = None # conanfile_class.options
        if options is None:
            packages.append(Package(conanfile_class.name, version_dir, version, dict()))
        else:
            optkeys = options.keys()
            for values in product(*options.values()):
                options_v2 = dict(zip(optkeys, values))
                packages.append(Package(conanfile_class.name, version_dir, version, options_v2))

    return packages


def get_windows_conf(package: Package, options: str):
    return {
        'stage': 'build',
        'resource_group': f'choco-{random.randint(0, 4)}',
        'extends': ['.shared_windows_runners'],
        'cache': {
            'key': '${CI_COMMIT_REF_SLUG}',
            'paths': [
                'C:\\Users\\gitlab_runner\\AppData\\Local\\Temp\\chocolatey'
            ]
        },
        'before_script': [
            'choco install -y cmake',
            'choco install python --version=3.8.0 -y'
        ],
        'script': [
            '$env:PATH+=";C:\\Python38;C:\\Python38\\Scripts;C:\\Program Files\\CMake\\bin"',
            'pip install conan',
            "conan --version",
            "conan config install https://github.com/conan-io/hooks.git",
            "set CONAN_HOOK_ERROR_LEVEL=30",
            "conan config set hooks.conan-center",
            f"./conan_runner.py create recipes/{package.name}/{package.subfolder} {package.name}/{package.version}@ --build=missing {options}"
        ]
    }


def get_linux_conf(package: Package, docker_image: str, options: str):
    return {
        'image': docker_image,
        'stage': package.name,
        'variables': {
            "GIT_SUBMODULE_STRATEGY": "recursive"
        },
        'before_script': [
            'export DEBIAN_FRONTEND=noninteractive',
            'apt-get update -y',
            'apt-get install -y python3 python3-pip build-essential cmake git',
            'pip3 install conan',
            'conan config install https://github.com/conan-io/hooks.git',
            'conan config set hooks.conan-center',
        ],

        'script': [
            "export CONAN_HOOK_ERROR_LEVEL=30",
            "conan --version",
            f"python3 ./conan_runner.py create conan-center-index/recipes/{package.name}/{package.subfolder} {package.name}/{package.version}@ --build=missing {options}"
        ]
    }


if __name__ == '__main__':
    gitlab_ci = {
        # 'stages': ['build'],
        '.shared_windows_runners': {
            'tags': [
                'shared-windows',
                'windows',
                'windows-1809'
            ]
        }
    }

    # arg_parser = argparse.ArgumentParser()
    # arg_parser.add_argument("--recipe-dir", help="directory containing the recipe", required=True)
    # arg_parser.add_argument("--config", help="config with build settings", required=True)
    # args = arg_parser.parse_args()

    stages = set()
    for subdir, dirs, files in os.walk("conan-center-index/recipes"):
        for recipe_directory in dirs:
            full_path_to_recipe_directory = os.path.join("conan-center-index/recipes", recipe_directory)
            try:
                all_packages = get_all_packages(full_path_to_recipe_directory)
            except:
                continue

            for package in all_packages:
                formatted_options = ' '.join(f'-o {package.name}:{o}={v}' for o, v in package.options.items())
                stages.add(package.name)
                if False:
                    gitlab_ci[f'win-{full_path_to_recipe_directory}/{package.version} {formatted_options}'] = \
                        get_windows_conf(package, formatted_options)

                for docker_image in ["ubuntu"]:
                    gitlab_ci[f'linux-{docker_image}-{package.name}/{package.version} {formatted_options}'] = \
                        get_linux_conf(package, docker_image, formatted_options)

    gitlab_ci['stages'] = list(stages)
    json = json.dumps(gitlab_ci, indent=2)
    print(json)
