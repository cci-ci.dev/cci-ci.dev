import os
from jinja2 import Environment, PackageLoader
from markdown2 import markdown


def get_posts(posts_directory):
    posts = []
    for subdir, dirs, files in os.walk(posts_directory):
        for post_filename in files:
            post_path = f'{posts_directory}/{post_filename}'
            with open(post_path) as post_md_file:
                post_data = markdown(post_md_file.read(), extras=['metadata'])
                post = {
                    "content": post_data,
                    "meta": post_data.metadata,
                    "filename": os.path.splitext(post_filename)[0]
                }
                posts.append(post)

    return posts

if __name__ == '__main__':
    output_directory = 'generated_html'
    if not os.path.exists(output_directory):
        os.makedirs(output_directory)

    posts = get_posts('content')

    env = Environment(loader=PackageLoader('main', 'template'))
    home_template = env.get_template('index.html')
    home_html = home_template.render(posts=posts)

    with open(f'{output_directory}/index.html', 'w') as file:
        file.write(home_html)

    content_template = env.get_template('post.html')

    for post in posts:
        with open(f'{output_directory}/{post["filename"]}.html', 'w') as post_file:
            post_html = content_template.render(post=post)
            post_file.write(post_html)
