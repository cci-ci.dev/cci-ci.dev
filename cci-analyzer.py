#!/usr/bin/env python3
from conans.model.conan_file import ConanFile
from dataclasses import dataclass, asdict
from importlib import import_module
from typing import Dict, List
import argparse
import inspect
import json
import os
import subprocess
import yaml
from sys import stderr


@dataclass(frozen=True, eq=True)
class Recipe:
    options: Dict[str, str]
    requires: List[str]
    default_options: Dict[str, str]


@dataclass(frozen=True, eq=True)
class Package:
    name: str
    version: str
    recipe: Recipe


@dataclass(frozen=True, eq=True)
class ScanResult:
    git_version: str
    packages: List[Package]


def analyze_cci(cci_path) -> ScanResult:
    git_version = subprocess.check_output(['git', 'rev-parse', 'HEAD'], cwd=cci_path).decode('ascii').strip()

    recipes_path = os.path.join(cci_path, "recipes")
    packages: List[Package] = list()

    for package_name in sorted(os.listdir(recipes_path)):
        path_to_package = os.path.join(recipes_path, package_name)

        if not os.path.isdir(path_to_package):
            continue

        if package_name.__contains__('.'):
            print(f"skip: {package_name}", file=stderr)
            continue

        path_to_config = os.path.join(path_to_package, 'config.yml')
        recipe_config = yaml.load(open(path_to_config), Loader=yaml.FullLoader)

        expected_package_directories = set()
        real_package_directories = set(filter(lambda d: os.path.isdir(os.path.join(path_to_package, d)),
                                              os.listdir(path_to_package)))
        for package_version, version_data in recipe_config['versions'].items():
            recipe_directory = version_data['folder']
            expected_package_directories.add(recipe_directory)
            if recipe_directory.__contains__('.'):
                print(f"skip: {package_name}/{recipe_directory} [{package_version}]", file=stderr)
                continue

            path_to_conanfile = f"{cci_path}.recipes.{package_name}.{recipe_directory}.conanfile"
            python_code = import_module(path_to_conanfile)
            conanfile_class = None

            for name, obj in inspect.getmembers(python_code):
                if inspect.isclass(obj) and ConanFile in obj.__bases__:
                    conanfile_class = obj
            assert conanfile_class is not None

            options = dict() if conanfile_class.options is None else conanfile_class.options
            requires = list() if not hasattr(conanfile_class, 'requires') else conanfile_class.requires
            default_options = dict() if conanfile_class.default_options is None else conanfile_class.default_options

            packages.append(Package(
                name=package_name,
                version=package_version,
                recipe=Recipe(
                    requires=requires,
                    default_options=default_options,
                    options=options
                )
            ))

        diff_package_directories = real_package_directories ^ expected_package_directories
        if len(diff_package_directories) != 0:
            print(f"warn: {package_name} unknown directories: {diff_package_directories}", file=stderr)

    return ScanResult(git_version=git_version, packages=packages)


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--path', type=str, default="conan-center-index", help='path to cci')
    args = parser.parse_args()
    scan_result: ScanResult = analyze_cci(args.path)

    print(json.dumps(asdict(scan_result), indent=1))
