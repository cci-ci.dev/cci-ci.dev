use rayon::prelude::*;
use serde::{Deserialize, Serialize};
use std::borrow::Borrow;
use std::collections::{HashMap, HashSet};
use std::env;
use std::fmt::Formatter;
use std::fs::File;
use std::io::BufReader;
use std::path::Path;
use std::process;
use std::sync::Arc;
use tabled::{Tabled, Table, builder::Builder, Style};


#[derive(Serialize, Deserialize, Clone, Tabled)]
#[serde(untagged)]
enum OptionArrayTypes {
    Bool(bool),
    Int(i64),
    OptionalString(Option<String>),
}


#[derive(Serialize, Deserialize, Clone)]
#[serde(untagged)]
enum OptionType {
    String(String),
    Vec(Vec<OptionArrayTypes>),
}


#[derive(Serialize, Deserialize, Clone)]
#[serde(untagged)]
enum ConcreteOptionType {
    String(String),
    OptionArrayTypes(OptionArrayTypes),
}
impl std::fmt::Display for ConcreteOptionType {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        match self {
            ConcreteOptionType::String(data) =>
                write!(f, "{}", data),
            ConcreteOptionType::OptionArrayTypes(data) => {
                match data {
                    OptionArrayTypes::Bool(data) =>
                        write!(f, "{}", data),
                    OptionArrayTypes::Int(data) =>
                        write!(f, "{}", data),
                    OptionArrayTypes::OptionalString(data) =>
                        write!(f, "{}", data.as_deref().unwrap_or("None"))
                }
            }
        }
    }
}


#[derive(Serialize, Deserialize, Clone)]
struct Recipe {
    options: HashMap<String, OptionType>,
    // requires: Vec<String>,
    // default_options: HashMap<String, HashMap<String, OptionType>>,
}


#[derive(Serialize, Deserialize, Clone)]
struct Package {
    name: String,
    version: String,
    recipe: Recipe
}


#[derive(Serialize, Deserialize, Clone)]
struct ScanResult {
    git_version: String,
    packages: Vec<Package>
}


#[derive(Serialize, Deserialize, Clone)]
struct PackageProduct {
    name: String,
    version: String,
    options: HashMap<String, ConcreteOptionType>,
}

fn process_package(package: &Package) -> usize {
    let max_deep_level = package.recipe.options.iter().map(|option|
        match option.1 {
            OptionType::String(data) => 1,
            OptionType::Vec(options) => options.len(),
        }
    ).max().unwrap_or(0);

    let mut products = vec![];
    for deep_level in 0..max_deep_level {
        let options = package.recipe.options
            .iter()
            .fold(HashMap::new(), |mut map, option|{
                match option.1 {
                    OptionType::String(data) => {
                        ConcreteOptionType::String(data.clone());
                        map.insert(option.0.clone(), ConcreteOptionType::String(data.clone()));
                        return map;
                    },
                    OptionType::Vec(data) => {
                        let index = deep_level % data.len();
                        let value = data.get(index).unwrap();
                        let value = ConcreteOptionType::OptionArrayTypes(value.clone());
                        map.insert(option.0.clone(), value.clone());
                        return map;
                    }
                }
            });
        products.push(PackageProduct {
            name: package.name.clone(),
            version: package.version.clone(),
            options: options
        });
    }


    let records = products.iter().fold(Vec::new(),|mut records, product| {
        let record = package.recipe.options.iter().fold(Vec::new(), |mut acc, option| {
            let cell = product.options.get(option.0).unwrap();
            acc.push(cell);
            return acc;
        });
        records.push(record);
        return records;
    });


    let columns:Vec<String> = package.recipe.options.keys().cloned().collect();
    let table = Builder::from_iter(records.iter().as_slice())
        .set_columns(columns.clone())
        .index()
        .build()
        .with(Style::modern());


    println!("{}/{}: deep level {}, options count {}",
             package.name,
             package.version,
             max_deep_level,
             package.recipe.options.len());
    println!("{}", table.to_string());

    return records.len();
}


fn main() {
    let args: Vec<String> = env::args().collect();
    if args.len() != 2 {
        process::exit(1);
    }

    let path_to_json = &args[1];
    let json_file_path = Path::new(path_to_json);
    let json_file = File::open(json_file_path).expect("file not found");
    let reader = BufReader::new(json_file);

    let scan_result: ScanResult = serde_json::from_reader(reader)
        .expect("error while reading json");

    let packages_names: HashSet<String> = scan_result.packages
        .iter()
        .map(|p| p.name.clone())
        .collect();

    let builds: usize = scan_result.packages.iter().map(process_package).sum();

    println!("");
    println!("Packages count: {}", packages_names.len());
    println!("References count: {}", scan_result.packages.len());
    println!("Builds count: {}", builds);

}


#[cfg(test)]
mod tests {
    use crate::*;

    #[test]
    fn parse() {
        let raw_json = r#"
            {
             "git_version": "50684852b12c415daaba3598631b126ac111978e",
             "packages": [{
                    "name":"aaplus",
                    "version":"2.41",
                    "recipe":{
                        "options":{
                            "shared":[true,false],
                            "fPIC":[true,false]
                        }
                    }
                }
             ]
            }
        "#;

        let scan_result: ScanResult = serde_json::from_str(raw_json).unwrap();
    }

    #[test]
    fn builds() {
        let package = Package{
            name: "some name".to_string(),
            version: "Some version".to_string(),
            recipe: Recipe{
                options: HashMap::from([
                    ("option_1".to_string(), OptionType::String("some_value".to_string())),
                    ("option_2".to_string(), OptionType::Vec(vec![
                        OptionArrayTypes::Bool(true),
                        OptionArrayTypes::Bool(false),
                        OptionArrayTypes::OptionalString(Some("NotNone".to_string())),
                        OptionArrayTypes::OptionalString(None),
                        OptionArrayTypes::OptionalString(None),
                    ])),
                ])
            }
        };
        let builds = process_package(&package);
        assert_eq!(builds, 5);
    }
}
